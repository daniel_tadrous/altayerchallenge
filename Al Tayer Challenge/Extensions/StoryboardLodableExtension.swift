//
//  StoryboardLodableExtension.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

protocol ProductsViewControllerStoryboardLoadable: StoryboardLodable {
}
protocol ProductDetailsViewControllerStoryboardLoadable: StoryboardLodable {
}


extension ProductsViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Products"
    }
}
extension ProductDetailsViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "ProductDetails"
    }
}
