//
//  StringExtension.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation

extension String{
    func getDate(format:String)->Date?{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format
        return dateFormatterGet.date(from:self)
    }
    
    func getAttributedString()->NSAttributedString{
        if let htmlData = self.data(using: String.Encoding.unicode) {
            do {
                let attributedText = try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                return attributedText
            } catch {
                
            }
        }
        return NSAttributedString(string: "")
    }
}
