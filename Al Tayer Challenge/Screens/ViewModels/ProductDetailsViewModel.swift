//
//  ProductDetailsViewModel.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class ProductDetailsViewModel{
    
    private var service: IProductsService
    private let disposeBag = DisposeBag()
    let product = Variable<Product?>(nil)
    let sku = Variable<String?>(nil)
    
    init(service: IProductsService) {
        self.service = service
        // observe the input product to send the add product request
        self.sku.asObservable().subscribe(onNext: { [unowned self](prodSku) in
            if prodSku != nil{
                self.getProduct()
            }
        }).disposed(by: disposeBag)
    }
    
    private func getProduct(){
        self.service.getProductDetails(sku: self.sku.value!).subscribe(onNext: { [unowned self](product) in
            if let product = product{
                self.product.value = product
            }
        }).disposed(by: disposeBag)
    }
}
