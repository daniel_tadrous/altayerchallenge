//
//  ProductsViewModel.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class ProductsViewModel{
    
    private var service: IProductsService
    private let disposeBag = DisposeBag()
    let products = Variable<[Product]?>(nil)
    let page = Variable<Int?>(nil)
    var categoryName: String!
    var pagination: Pagination!
    
    init(service: IProductsService) {
        self.service = service
        self.page.asObservable().subscribe(onNext: { [unowned self] (p) in
            if p != nil{
                self.getProducts()
            }
        }).disposed(by: disposeBag)
    }
    private func getProducts(){
        self.service.getProducts(page: page.value!).subscribe(onNext: { [unowned self] (productsResponse) in
            if let productsResponse = productsResponse{
                self.categoryName = productsResponse.categoryName
                self.pagination = productsResponse.pagination
                var prodAr = [Product]()
                if self.products.value != nil{
                    prodAr.append(contentsOf: self.products.value!)
                }
                prodAr.append(contentsOf: productsResponse.hits!)
                self.products.value = prodAr
            }
        }).disposed(by: disposeBag)
    }
   
}
