//
//  ProductDetailsCollectionViewCell.swift
//  Al Tayer Challenge
//
//  Created by innuva on 10/23/18.
//  Copyright © 2018 daniel. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var minPriceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel?
    @IBOutlet weak var categoryNameLbl: UILabel?
    @IBOutlet weak var priceLbl: UILabel?
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var imageSlider: ImageSlideshow?
    func setData(product:Product){
        minPriceLbl.text = "\(product.minPrice ?? 0) \(L10n.currency)"
        titleLbl.text = product.name
        descriptionLbl?.attributedText = product.description?.getAttributedString()
        categoryNameLbl?.text = product.designerCategoryName
        priceLbl?.text = "\(product.price ?? 0) \(L10n.currency)"
       
        
        if let imageSlider = imageSlider{
            self.imageSlider?.contentScaleMode = .scaleAspectFill
            let pageIndicator = UIPageControl()
            pageIndicator.currentPageIndicatorTintColor = UIColor.orange
            pageIndicator.pageIndicatorTintColor = UIColor.white
            imageSlider.pageIndicator = pageIndicator
            var count = product.media!.count
            var imageSources = [ImageSource]()
            for media in product.media!{
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: media.getImageUrl()), progress: nil, completed: { (image, data, error, done) in
                    count = count - 1
                    
                    if let image = image{
                        imageSources.append(ImageSource(image: image))
                        if count == 0{
                            self.imageSlider?.setImageInputs(imageSources)
                        }
                    }
                })
            }
        }
        if (product.media?.count ?? 0) > 0{
            self.imageView?.sd_setImage(with: URL(string: product.media![0].getImageUrl()), completed: nil)
        }
    }
}
