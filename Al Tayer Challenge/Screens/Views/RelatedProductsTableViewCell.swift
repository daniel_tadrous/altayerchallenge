//
//  RelatedProductsTableViewCell.swift
//  Al Tayer Challenge
//
//  Created by innuva on 10/23/18.
//  Copyright © 2018 daniel. All rights reserved.
//

import UIKit

class RelatedProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    private var products = [Product]()
    let relatedProductCellID = "RelatedProductCollectionViewCell"
    let collectionCellWidth = 150
    func setData(products: [Product]){
        self.collectionView.register(UINib(nibName: relatedProductCellID, bundle: nil), forCellWithReuseIdentifier: relatedProductCellID)
        self.products = products
    }
}
extension RelatedProductsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = products[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: relatedProductCellID, for: indexPath) as! ProductCollectionViewCell
        cell.setData(product: product)
        return cell      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.width)
        let dim = self.collectionView.frame.width / 2 - 30
        size.width = dim
        size.height = dim * 1.2
        return size
       
    }
}
