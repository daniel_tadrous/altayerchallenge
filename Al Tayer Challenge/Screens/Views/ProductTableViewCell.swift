//
//  ProductTableViewCell.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage


class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var minPriceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
   
    @IBOutlet weak var imageSlider: ImageSlideshow!
    
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var arrowImage: UIImageView?
    
    private var onCollapseExpand: (()-> Void)?
    @IBAction func collapseExpandClick(_ sender: UIButton) {
        self.onCollapseExpand?()
    }
    func setData(product:Product,onCollapseExpand:(()->Void)? = nil){
        self.imageSlider.contentScaleMode = .scaleAspectFill
        self.onCollapseExpand = onCollapseExpand
        minPriceLbl.text = "\(product.minPrice ?? 0) \(L10n.currency)"
        titleLbl.text = product.name
        self.descriptionHeightConstraint?.priority = product.isDescriptionExpanded ? .defaultLow : .defaultHigh
        self.arrowImage?.image = product.isDescriptionExpanded ? #imageLiteral(resourceName: "up_arrow") : #imageLiteral(resourceName: "down_arrow")
        descriptionLbl.attributedText = product.description?.getAttributedString()
        categoryNameLbl.text = product.designerCategoryName
        priceLbl.text = "\(product.price ?? 0) \(L10n.currency)"
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.orange
        pageIndicator.pageIndicatorTintColor = UIColor.white
        imageSlider.pageIndicator = pageIndicator
        var count = product.media!.count
        var imageSources = [ImageSource]()
        for media in product.media!{
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: media.getImageUrl()), progress: nil, completed: { (image, data, error, done) in
                count = count - 1
                
                if let image = image{
                    imageSources.append(ImageSource(image: image))
                    if count == 0{
                        self.imageSlider.setImageInputs(imageSources)
                    }
                }
            })
        }
        
        
    }
}
