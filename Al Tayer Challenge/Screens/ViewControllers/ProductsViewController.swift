//
//  ProductsViewController.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxSwift

protocol ProductsDelegate {
    func openProductDetails(for sku: String)
}

class ProductsViewController: UIViewController, ProductsViewControllerStoryboardLoadable {

   
    @IBOutlet weak var productsTableView: UITableView!
    let cellIdentifier = "ProductTableViewCell"
    var viewModel: ProductsViewModel!
    var delegate: ProductsDelegate!
    private let disposeBag = DisposeBag()
    private var products: [Product] = []{
        didSet{
           self.productsTableView.reloadData()
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productsTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        viewModel.products.asObservable().subscribe(onNext: { [unowned self] (products) in
            if let products = products{
                self.products = products
                self.title = self.viewModel.categoryName
            }
        }).disposed(by: disposeBag)
        self.viewModel.page.value = 0
    }
    
}
extension ProductsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = self.products[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ProductTableViewCell
        cell.setData(product: product)
        if indexPath.row == self.products.count - 1{
            if self.viewModel.page.value! < self.viewModel.pagination.totalPages! - 1{
                self.viewModel.page.value = self.viewModel.page.value! + 1
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.products[indexPath.row]
        self.delegate.openProductDetails(for: product.sku!)
    }
}

