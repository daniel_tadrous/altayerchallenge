//
//  ProductDetailsViewController.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ImageSlideshow
import SDWebImage

class ProductDetailsViewController: UIViewController, ProductDetailsViewControllerStoryboardLoadable {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: ProductDetailsViewModel!
    let detailsCellID = "ProductDetailsTableViewCell"
    let relatedProductCellID = "RelatedProductsTableViewCell"
    let headersHeight = CGFloat(40.0)
    private let disposeBag = DisposeBag()
    
    private var product: Product!{
        didSet{
            self.relatedProducts = (self.product.relatedProductsLookup?.values.compactMap({ (p) -> Product in
                return p
            }))!
            self.tableView.reloadData()
        }
    }
    private var relatedProducts = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: detailsCellID, bundle: nil), forCellReuseIdentifier: detailsCellID)
        self.tableView.register(UINib(nibName: relatedProductCellID, bundle: nil), forCellReuseIdentifier: relatedProductCellID)
        self.viewModel.product.asDriver().drive(onNext: { [unowned self](product) in
            if let product = product{
                self.product = product
            }
        }).disposed(by: disposeBag)
    }
    
 
}
extension ProductDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.product != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: detailsCellID, for: indexPath) as! ProductTableViewCell
            cell.setData(product: self.product,onCollapseExpand: {
                self.product.isDescriptionExpanded = !self.product.isDescriptionExpanded
                tableView.reloadRows(at: [indexPath], with: .none)
            })
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: relatedProductCellID, for: indexPath) as! RelatedProductsTableViewCell
            cell.setData(products: self.relatedProducts)
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 && self.product != nil{
            
            return headersHeight
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: self.tableView.frame.width - 20, height: headersHeight
        ))
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.text = L10n.relatedProductsHeader
        view.addSubview(label)
        return view
    }
}
