//
//  BaseService.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

// add other end points here
enum EUrl: String{
    
    case PRODUCTS = "api/women/clothing", PRODUCT_DETAILS = "product/findbyslug?slug=", LISTING_IMAGE = "small_light(p=listing2x,of=jpg,q=70)/pub/media/catalog/product", DETAILS_IMAGE = "small_light(p=zoom,of=jpg,q=70)/pub/media/catalog/product"
    
}
class BaseService{
    
    // you can change base url in info.plist file
    private var baseUrl: String{
        get{
            return (Bundle.main.object(forInfoDictionaryKey: "base_url") as? String)!
        }
    }
    
    
    /// to get full url path
    ///
    /// - Parameter type: is the type of the endpoint used
    /// - Returns: full path
    func fullPathFor(type:EUrl) -> String {
        return "\(baseUrl)/\(type.rawValue)"
    }
    
    
    /// basic get request
    ///
    /// - Parameter url: full url for the endpoint
    /// - Returns: observable of generic object
    func getRequest<T>( url: String) -> Observable<T?> where T: Mappable{
        
        return Observable<T?>.create { (observer) -> Disposable in
            let requestReference = Alamofire.request(url, method: .get)
                .validate()
                .responseObject { (response: DataResponse<T>) in
                    switch response.result{
                    case .success:
                        observer.onNext(response.result.value)
                        observer.onCompleted()
                    case .failure:
                        observer.onError(response.error!)
                        observer.onCompleted()
                    }
            }
            return Disposables.create(with: {
                requestReference.cancel()
            })
        }
    }
}

