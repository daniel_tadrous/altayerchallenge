//
//  Media.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct Media : Mappable {
	var position : Int?
	var mediaType : String?
	var src : String?
	var videoUrl : String?

	init?(map: Map) {

	}
    private var baseUrl: String{
        get{
            return (Bundle.main.object(forInfoDictionaryKey: "image_base_url") as? String)!
        }
    }
    func getImageUrl(forListing: Bool = true)->String{
        let part = forListing ? EUrl.LISTING_IMAGE.rawValue : EUrl.DETAILS_IMAGE.rawValue
        return "\(baseUrl)/\(part)\(src!)"
    }
	mutating func mapping(map: Map) {

		position <- map["position"]
		mediaType <- map["mediaType"]
		src <- map["src"]
		videoUrl <- map["videoUrl"]
	}

}
