//
//  Product.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct Product : Mappable {
	var productId : Int?
	var sku : String?
	var season : String?
	var description : String?
	var name : String?
	var atgColorCode : String?
	var motherReference : String?
	var slug : String?
	var extraBadges : [String]?
	var image : String?
	var thumbnail : String?
	var swatchImage : String?
	var sizeAndFit : String?
	var price : Int?
	var color : String?
	var colorId : Int?
	var sizeCode : String?
	var sizeCodeId : Int?
	var sleeveLength : String?
	var parentSku : String?
	var styleColorId : String?
	var colorIdOriginal : Int?
	var colorIdString : String?
	var categoryIds : [Int]?
	var media : [Media]?
	var likes : Int?
	var minPrice : Int?
	var isInStock : Bool?
	var isInHomeDeliveryStock : Bool?
	var isInClickAndCollectStock : Bool?
	var categoryNamesInEnglish : [String]?
	var recommendationCategoryNames : [String]?
	var recommendationCategoryNamesEN : [String]?
	var designerCategoryName : String?
	var designerCategoryUrl : String?
	var discounted : String?
	var isClearance : Bool?
	var isPromotion : Int?
	var simpleType : String?
	var sameColorSiblings : [String]?
	var areAnyOptionsInStock : Bool?
	var visibleSku : String?
	var priceMatchUrl : String?
	var badges : [String]?
	var amberPointsPerItem : Int?
	var staticInfo : String?
	var vatInfo : String?
	var deepLink : String?
	var deepLinkFacebook : String?
	var isInWishList : Bool?
    var relatedProductsLookup: [String: Product]?
    var isDescriptionExpanded = false
    
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		productId <- map["productId"]
		sku <- map["sku"]
		season <- map["season"]
		description <- map["description"]
		name <- map["name"]
		atgColorCode <- map["atgColorCode"]
		motherReference <- map["motherReference"]
		slug <- map["slug"]
		extraBadges <- map["extraBadges"]
		image <- map["image"]
		thumbnail <- map["thumbnail"]
		swatchImage <- map["swatchImage"]
		sizeAndFit <- map["sizeAndFit"]
		price <- map["price"]
		color <- map["color"]
		colorId <- map["colorId"]
		sizeCode <- map["sizeCode"]
		sizeCodeId <- map["sizeCodeId"]
		sleeveLength <- map["sleeveLength"]
		parentSku <- map["parentSku"]
		styleColorId <- map["styleColorId"]
		colorIdOriginal <- map["colorIdOriginal"]
		colorIdString <- map["colorIdString"]
		categoryIds <- map["categoryIds"]
		media <- map["media"]
		likes <- map["likes"]
		minPrice <- map["minPrice"]
		isInStock <- map["isInStock"]
		isInHomeDeliveryStock <- map["isInHomeDeliveryStock"]
		isInClickAndCollectStock <- map["isInClickAndCollectStock"]
		categoryNamesInEnglish <- map["categoryNamesInEnglish"]
		recommendationCategoryNames <- map["recommendationCategoryNames"]
		recommendationCategoryNamesEN <- map["recommendationCategoryNamesEN"]
		designerCategoryName <- map["designerCategoryName"]
		designerCategoryUrl <- map["designerCategoryUrl"]
		discounted <- map["discounted"]
		isClearance <- map["isClearance"]
		isPromotion <- map["isPromotion"]
		simpleType <- map["simpleType"]
		sameColorSiblings <- map["sameColorSiblings"]
		areAnyOptionsInStock <- map["areAnyOptionsInStock"]
		visibleSku <- map["visibleSku"]
		priceMatchUrl <- map["priceMatchUrl"]
		badges <- map["badges"]
		amberPointsPerItem <- map["amberPointsPerItem"]
		staticInfo <- map["staticInfo"]
		vatInfo <- map["vatInfo"]
		deepLink <- map["deepLink"]
		deepLinkFacebook <- map["deepLinkFacebook"]
		isInWishList <- map["isInWishList"]
        relatedProductsLookup <- map["relatedProductsLookup"]
	}

}
