//
//  ResponseError.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

enum EErrorType:String{
    case INVALID_GRANT = "invalid_grant", UNDEFINED="undefined"
}
class ResponseError: Error,Mappable {
    
    var error: String?
    var errorDescription: String?
    var type: EErrorType?
    
    required init?(map: Map){
        self.type = EErrorType(rawValue: map.JSON["error"] as? String ?? "undefined")
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        errorDescription <- map["error_description"]
    }
}
