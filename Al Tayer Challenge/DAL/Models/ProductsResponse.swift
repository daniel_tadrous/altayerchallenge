//
//  ProductsResponse.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductsResponse : Mappable {
	var hits : [Product]?
	var pagination : Pagination?
	var routeType : String?
	var hitsPerPage : Int?
	var page : Int?
	var categoryName : String?
	

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		hits <- map["hits"]
		pagination <- map["pagination"]
		routeType <- map["routeType"]
		hitsPerPage <- map["hitsPerPage"]
		page <- map["page"]
		categoryName <- map["categoryName"]
	}

}
