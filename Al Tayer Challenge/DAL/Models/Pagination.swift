//
//  Pagination.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct Pagination : Mappable {
	var totalHits : Int?
	var totalPages : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		totalHits <- map["totalHits"]
		totalPages <- map["totalPages"]
	}

}
