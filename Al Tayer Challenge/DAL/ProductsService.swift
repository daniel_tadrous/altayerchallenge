//
//  ProductsService.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire

protocol IProductsService{
    /// get the product list
    ///
    /// - Returns: observable of optional product list
    func getProducts(page:Int)->Observable<ProductsResponse?>
    /// add a new product
    ///
    /// - Parameter product: product object
    /// - Returns: observable of optional product of the added one
    func getProductDetails(sku: String) -> Observable<Product?>
}

class ProductsService:BaseService, IProductsService{
    
    func getProducts(page:Int) -> Observable<ProductsResponse?> {
        let strUrl = "\(fullPathFor(type: .PRODUCTS))?p=\(page)"
        return getRequest(url: strUrl)
    }
    
    func getProductDetails(sku: String) -> Observable<Product?> {
        let strUrl = "\(fullPathFor(type: .PRODUCT_DETAILS))\(sku)"
        return getRequest(url: strUrl)
    }
    
}
