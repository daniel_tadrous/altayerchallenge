//
//  AutoRegisterer.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import Swinject

class AutoRegisterer{
    
    class func register(container:Container) {
        
        // services
       
        container.autoregister(IProductsService.self, initializer: ProductsService.init)
       
        // viewModels
        container.autoregister(ProductsViewModel.self, initializer: ProductsViewModel.init)
        container.autoregister(ProductDetailsViewModel.self, initializer: ProductDetailsViewModel.init)
        
        // viewControllers
        container.storyboardInitCompleted(ProductsViewController.self) { r, c
            in c.viewModel = r.resolve(ProductsViewModel.self)
        }
        container.storyboardInitCompleted(ProductDetailsViewController.self) { r, c
            in c.viewModel = r.resolve(ProductDetailsViewModel.self)
        }
    }
}
