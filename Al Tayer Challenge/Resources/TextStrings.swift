//
//  TextStrings.swift
//  Al Tayer Challenge
//
//  Created by Daniel Tadrous on 10/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation


enum L10n {
    
    // name for general table
    private static let t1 = "Localizable"
    static var productTitle:String {
        return L10n.tr(t1,"product_title")
        
    }
    static var currency: String{
        return L10n.tr(t1, "currency")
    }
    static var relatedProductsHeader: String{
        return L10n.tr(t1, "related_products_header")
    }
    static func getStringFor(key: String) -> String {
        return L10n.tr(t1,key)
        
    }
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
